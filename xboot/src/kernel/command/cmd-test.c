/*
 * kernel/command/cmd-test.c
 */

#include <command/command.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include <stdio.h>
#include <framebuffer/framebuffer.h>

#include <cairo-xboot.h>
#include <cairo-ft.h>
#include <shell/ctrlc.h>
#include <input/input.h>
#include <input/keyboard.h>
#include "lv_examples/lv_apps/demo/demo.h"
#include "lv_examples/lv_apps/benchmark/benchmark.h"
//#include "lv_examples/lv_tests/lv_test_theme/lv_test_theme.h"
#include "lvgl/lvgl.h"

//extern struct render_t * fb_create(struct framebuffer_t * fb);
//extern void fb_destroy(struct framebuffer_t * fb, struct render_t * render);

/**********************
 *  STATIC PROTOTYPES
 **********************/
static void ex_disp_flush(int32_t x1, int32_t y1, int32_t x2, int32_t y2, const lv_color_t * color_p);
static void ex_disp_map(int32_t x1, int32_t y1, int32_t x2, int32_t y2, const lv_color_t * color_p);
static void ex_disp_fill(int32_t x1, int32_t y1, int32_t x2, int32_t y2,  lv_color_t color);
#if USE_LV_GPU
static void ex_mem_blend(lv_color_t * dest, const lv_color_t * src, uint32_t length, lv_opa_t opa);
static void ex_mem_fill(lv_color_t * dest, uint32_t length, lv_color_t color);
#endif
static bool ex_tp_read(lv_indev_data_t *data);
//--------------------------------------------------------------------------------------------------
static bool left_button_down = false;
static int16_t last_x = 0;
static int16_t last_y = 0;

struct event_t e;              

bool mouse_read(lv_indev_data_t * data)
{
    /*Store the collected data*/
    mouse_handler();
    data->point.x = last_x;
    data->point.y = last_y;
    data->state = left_button_down ? LV_INDEV_STATE_PR : LV_INDEV_STATE_REL;

    return false;
}
void mouse_handler()
{
    
    if(pump_event(runtime_get()->__event_base, &e))
        {
            if(e.type == EVENT_TYPE_KEY_UP)
            {
                //if(e.e.key_up.key == KEY_EXIT)
                //    break;
            }
            switch (e.type) {
            case EVENT_TYPE_MOUSE_UP:
                if (e.e.mouse_up.button == MOUSE_BUTTON_LEFT)
                    left_button_down = false;
                break;
            case EVENT_TYPE_MOUSE_DOWN:
                if (e.e.mouse_down.button == MOUSE_BUTTON_LEFT) {
                    left_button_down = true;
                    last_x = e.e.mouse_down.x;
                    last_y = e.e.mouse_down.y;
                }
                break;
            case EVENT_TYPE_MOUSE_MOVE:
                last_x = e.e.mouse_move.x;
                last_y = e.e.mouse_move.y;
                break;
            default: break;
            }
        }
}
struct lvgui_pdata_t
{
    struct timer_t timer;
    bool_t online;
};

static void usage(void)
{
    printf("usage:\r\n");
    printf("    test [args ...]\r\n");
}
/**********************
 *      MACROS
 **********************/

/**********************
 *   GLOBAL FUNCTIONS
 **********************/

/**********************
 *   STATIC FUNCTIONS
 **********************/
static int lvgui_timer_function(struct timer_t * timer, void * data)
{
    //struct lvgui_pdata_t * pdat = (struct lvgui_pdata_t *)(data);
    timer_forward_now(timer, ms_to_ktime(1));
    
    lv_tick_inc(1);    
    return 1;
}

/* Flush the content of the internal buffer the specific area on the display
 * You can use DMA or any hardware acceleration to do this operation in the background but
 * 'lv_flush_ready()' has to be called when finished
 * This function is required only when LV_VDB_SIZE != 0 in lv_conf.h*/
static void ex_disp_flush(int32_t x1, int32_t y1, int32_t x2, int32_t y2, const lv_color_t * color_p)
{
    /*The most simple case (but also the slowest) to put all pixels to the screen one-by-one*/
    struct framebuffer_t * fb;
    struct render_t * render;
    fb =search_first_framebuffer();
    render= fb_create(fb);
    //int point=0; //int m,n;
    //int width=fb->width;
    //int height=fb->height;
    int *pixels=render->pixels;

    int32_t x;
    int32_t y;

    uint32_t w = x2 - x1 + 1;
    // if (x1!=0)
    //  printf("x1:%d",x1);;
    for(y = y1; y <= y2; y++) {
        memcpy(&pixels[y * 640 + x1], color_p, w * sizeof(lv_color_t));
        color_p += w;
        //if (y%100==0) printf("y:%d",y);
        }
    
    fb->present(fb, render);
    fb_destroy(fb, render);


    /* IMPORTANT!!!
     * Inform the graphics library that you are ready with the flushing*/
    lv_flush_ready();
}

/* Write a pixel array (called 'map') to the a specific area on the display
 * This function is required only when LV_VDB_SIZE == 0 in lv_conf.h*/
static void ex_disp_map(int32_t x1, int32_t y1, int32_t x2, int32_t y2, const lv_color_t * color_p)
{
    /*The most simple case (but also the slowest) to put all pixels to the screen one-by-one*/

    int32_t x;
    int32_t y;
    for(y = y1; y <= y2; y++) {
        for(x = x1; x <= x2; x++) {
            /* Put a pixel to the display. For example: */
            /* put_px(x, y, *color_p)*/
            color_p++;
        }
    }
}

/* Write a pixel array (called 'map') to the a specific area on the display
 * This function is required only when LV_VDB_SIZE == 0 in lv_conf.h*/
static void ex_disp_fill(int32_t x1, int32_t y1, int32_t x2, int32_t y2,  lv_color_t color)
{
    /*The most simple case (but also the slowest) to put all pixels to the screen one-by-one*/

    int32_t x;
    int32_t y;
    for(y = y1; y <= y2; y++) {
        for(x = x1; x <= x2; x++) {
            /* Put a pixel to the display. For example: */
            /* put_px(x, y, *color)*/
        }
    }
}

#if USE_LV_GPU

/* If your MCU has hardware accelerator (GPU) then you can use it to blend to memories using opacity
 * It can be used only in buffered mode (LV_VDB_SIZE != 0 in lv_conf.h)*/
static void ex_mem_blend(lv_color_t * dest, const lv_color_t * src, uint32_t length, lv_opa_t opa)
{
    /*It's an example code which should be done by your GPU*/

    int32_t i;
    for(i = 0; i < length; i++) {
        dest[i] = lv_color_mix(dest[i], src[i], opa);
    }
}

/* If your MCU has hardware accelerator (GPU) then you can use it to fill a memory with a color
 * It can be used only in buffered mode (LV_VDB_SIZE != 0 in lv_conf.h)*/
static void ex_mem_fill(lv_color_t * dest, uint32_t length, lv_color_t color)
{
    /*It's an example code which should be done by your GPU*/

    int32_t i;
    for(i = 0; i < length; i++) {
        dest[i] = color;
    }
}

#endif

/* Read the touchpad and store it in 'data'
 * REaturn false if no more data read; true for ready again */
static bool ex_tp_read(lv_indev_data_t * data)
{
    /* Read your touchpad */
    /* data->state = LV_INDEV_STATE_REL or LV_INDEV_STATE_PR */
    /* data->point.x = tp_x; */
    /* data->point.y = tp_y; */
    //return 
    return false;   /*false: no more data to read because we are no buffering*/
}
static void hal_init(void)
{
    
     /***********************
     * Display interface
     ***********************/
    lv_disp_drv_t disp_drv;                         /*Descriptor of a display driver*/
    lv_disp_drv_init(&disp_drv);                    /*Basic initialization*/
    /*Set up the functions to access to your display*/
    disp_drv.disp_flush = ex_disp_flush;            /*Used in buffered mode (LV_VDB_SIZE != 0  in lv_conf.h)*/
    disp_drv.disp_fill = ex_disp_fill;              /*Used in unbuffered mode (LV_VDB_SIZE == 0  in lv_conf.h)*/
    disp_drv.disp_map = ex_disp_map;                /*Used in unbuffered mode (LV_VDB_SIZE == 0  in lv_conf.h)*/
    /*Finally register the driver*/
    lv_disp_drv_register(&disp_drv);
    /***********************
     * Tick interface
     ***********************/
    /* Initialize a Timer for 1 ms period and
     * in its interrupt call
     * lv_tick_inc(1); */
    struct lvgui_pdata_t * pdat;
    pdat = malloc(sizeof(struct lvgui_pdata_t));
    pdat->online = FALSE;
    timer_init(&pdat->timer, lvgui_timer_function, pdat);
    timer_start_now(&pdat->timer, ms_to_ktime(5));
    /*************************
     * Input device interface
     *************************/
    /*Add a touchpad in the example*/
    /*touchpad_init();*/                            /*Initialize your touchpad*/
    lv_indev_drv_t indev_drv;                       /*Descriptor of an input device driver*/
    lv_indev_drv_init(&indev_drv);                  /*Basic initialization*/
    indev_drv.type = LV_INDEV_TYPE_POINTER;         /*The touchpad is pointer type device*/
    indev_drv.read = mouse_read;                 /*Library ready your touchpad via this function*/
    lv_indev_drv_register(&indev_drv);              /*Finally register the driver*/
}
//---------------------------------------------------------------------------------------
static int do_test(int argc, char ** argv)
{
    /***********************
     * Initialize LittlevGL
     ***********************/
    lv_init();
    hal_init();

    //lv_obj_t * label = lv_label_create(lv_scr_act(), NULL);
    {
        /*Create a Label on the currently active screen*/
        lv_obj_t * label1 =  lv_label_create(lv_scr_act(), NULL);

        /*Modify the Label's text*/
        lv_label_set_text(label1, "Hello world!");

        /* Align the Label to the center
         * NULL means align on parent (which is the screen now)
         * 0, 0 at the end means an x, y offset after alignment*/
        lv_obj_align(label1, NULL, LV_ALIGN_CENTER, 0, 0);
        /*Add a button*/
        lv_obj_t * btn1 = lv_btn_create(lv_scr_act(), NULL);             /*Add to the active screen*/
        lv_obj_set_pos(btn1, 10, 10);                                    /*Adjust the position*/
       // lv_btn_set_action(btn1, LV_BTN_ACTION_CLICK, my_click_action);   /*Assign a callback for clicking*/

        /*Add text*/
        lv_obj_t * label = lv_label_create(btn1, NULL);                  /*Put on 'btn1'*/
        lv_label_set_text(label, "Click me");
    }
    /*Load a demo*/
    //demo_create();

    /*Or try the benchmark too to see the speed of your MCU*/
    benchmark_create();

    /*Or try a theme (Enable the theme in lv_conf.h with USE_LV_THEME_...  1 )*/
        //lv_theme_t * th = lv_theme_night_init(210, NULL);      /*Hue: 210; Font: NULL (default)*/
        //lv_test_theme_1(th);


    /*************************************
     * Run the task handler of LittlevGL
     *************************************/
    //while(1) {
        /* Periodically call this function.
         * The timing is not critical but should be between 1..10 ms */
        //}
    while(1)
    {
        
        lv_task_handler();
        //mouse_handler();
        mdelay(5);
    }
    return 0;
}

static struct command_t cmd_test = {
    .name   = "test",
    .desc   = "debug command for programmer",
    .usage  = usage,
    .exec   = do_test,
};

static __init void test_cmd_init(void)
{
    register_command(&cmd_test);
}

static __exit void test_cmd_exit(void)
{
    unregister_command(&cmd_test);
}

command_initcall(test_cmd_init);
command_exitcall(test_cmd_exit);
