#ifndef __STDBOOL_H

#define __STDBOOL_H

 

typedef enum{

    false = 0,

    true = !false,

}bool;

 

#endif
